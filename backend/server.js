const express = require('express');
const app = express();
const server = require('http').createServer(app);
const { MongoClient } = require('mongodb');
const io = require('socket.io')(server, {
  cors: {
    origin: '*',
  }
});

let connectedUsers = {};
const uri = "mongodb+srv://erikhpetrushynetspz2021:03.06.2004@cluster0.3p7ue1n.mongodb.net/";
const client = new MongoClient(uri);
let chatsCollection;
let usersCollection;

async function connectToDatabase() {
  try {
    await client.connect();
    const db = client.db();
    chatsCollection = db.collection('chats');
    usersCollection = db.collection('users');
    console.log('Connected to the database');
  } catch (e) {
    console.error(e);
  }
}
connectToDatabase();

io.on('connection', async (socket) => {
  console.log('A user connected');
  async function InsertUser(userName) {
    const User = {
      user: userName,
      chats: [],
      userID: socket.id
    };
    await usersCollection.insertOne(User);
  }

  async function ReturnUserID(userName) {
    const user = await usersCollection.findOne({ user: String(userName) });
    if (user) {
      return user.userID;
    } else {
      console.log('User not found');
    }
  }

  async function CheckIfUserExist(userName) {
    const user = await usersCollection.findOne({ user: String(userName) });
    return !!user;
  }

  async function InsertChatToUser(userName, IDOfChat, NameOfChat) {
    const user = await usersCollection.findOne({ user: String(userName) });
    if (user) {
      user.chats.push([IDOfChat, NameOfChat]);
      await usersCollection.updateOne({ user: String(userName) }, { $set: { chats: user.chats } });
      console.log('User updated successfully');
    } else {
      console.log('User not found');
    }
  }

  async function ReturnUserDoubleChatName(userName) {
    try {
      const user = await usersCollection.findOne({ user: String(userName) });
      if (user) {
        return user.chats;
      } else {
        console.log('User ' + userName + ' not found');
        return {};
      }
    } catch (error) {
      console.error('Error retrieving user chats:', error);
      throw error;
    }
  }

  async function ReturnUserChats(userName) {
    try {
      const user = await usersCollection.findOne({ user: String(userName) });
      if (user) {
        const firstElements = user.chats.map(chat => chat[0]); 
        const chats = await chatsCollection.find({ chatID: { $in: firstElements } }).toArray();
        console.log('User chats: ');
        console.log(user.chats);
        return chats;
      } else {
        console.log('User ' + userName + ' not found');
        return {};
      }
    } catch (error) {
      console.error('Error retrieving user chats:', error);
      throw error;
    }
  }

  async function InsertChat(IDOfChat) {
    const Chat = {
      chatID: String(IDOfChat),
      messages: [],
      users: [],
    };
    await chatsCollection.insertOne(Chat);
  }

  async function UpdateChat(IDOfChat, message) {
    const chat = await chatsCollection.findOne({ chatID: String(IDOfChat) });
    if (chat) {
      chat.messages.push(message);
      await chatsCollection.updateOne({ chatID: String(IDOfChat) }, { $set: { messages: chat.messages } });
      console.log('Chat updated successfully');
    } else {
      console.log('Chat not found');
    }
  }

  async function ChangeID(nameOfUser, IDOfUser) {
    try {
      const user = await usersCollection.findOne({ user: String(nameOfUser) });
      if (user) {
        await usersCollection.updateOne({ user: nameOfUser }, { $set: { userID: IDOfUser } });
        console.log('User updated successfully');
      } else {
        console.log('User is new!');
      }
    } catch (error) {
      console.error('Error adding user to chat:', error);
    }
  }

  async function AddUserToChat(IDOfChat, user) {
    try {
      const chat = await chatsCollection.findOne({ chatID: String(IDOfChat) });
      if (chat) {
        chat.users.push(user);
        await chatsCollection.updateOne({ chatID: String(IDOfChat) }, { $set: { users: chat.users } });
        console.log('Chat updated successfully');
      } else {
        console.log('Chat not found');
      }
    } catch (error) {
      console.error('Error adding user to chat:', error);
    }
  }

  async function CheckIfChatsExist(IDOfChat) {
    const chat = await chatsCollection.findOne({ chatID: String(IDOfChat) });
    return !!chat;
  }

  socket.on('setUserName', async (UserData) => {
    connectedUsers[UserData.name] = {
      id: UserData.id,
      name: UserData.name,
    };
    if (!await CheckIfUserExist(UserData.name)) {
      await InsertUser(UserData.name);
    } else {
      await ChangeID(UserData.name, UserData.id);
    }
    const userChats = await ReturnUserChats(UserData.name);
    for (let chat of userChats) {
      console.log("Added to chat: " + chat.chatID);
      socket.join(chat.chatID);
    }
    console.log('Name: ' + connectedUsers[UserData.name].name + ', ID: ' + connectedUsers[UserData.name].id);
    socket.emit('initialData', [userChats, await ReturnUserDoubleChatName(UserData.name)]);
  });

  socket.on('SendUpdateInfo', async (user) => {
    console.log('UserName: ' + user);
    socket.emit('initialData', [await ReturnUserChats(user), await ReturnUserDoubleChatName(user)]);
  });

  socket.on('joinRoom', async (Data) => {
    if (!await CheckIfChatsExist(Data[0])) {
      await InsertChat(Data[0]);
    }
    await socket.join(Data[0]);
    await UpdateChat(Data[0], {
      chatRoom: Data[0],
      content: 'User ' + Data[1] + ' connected!',
      sender: 'Chat'
    });
    await InsertChatToUser(Data[1], Data[0], Data[0]);
    await AddUserToChat(Data[0], Data[1]);
    await io.to(Data[0]).emit('updateInfo');
  });

  socket.on('sendMessage', async (message) => {
    console.log('Message on server: ');
    console.log(message);
    await UpdateChat(message.chatRoom, message);
    await io.to(message.chatRoom).emit('updateInfo');
  });

  socket.on('RequestUser', async (ToFrom) => {
    console.log('Request from: ' + ToFrom.From + " To: " + ToFrom.To);
    const chatName =  String(ToFrom.From) + String(ToFrom.To);
    const userId = await ReturnUserID(String(ToFrom.To));
    console.log('ID: ' + userId);
    await io.to(userId).emit('ChatRequest', { From: ToFrom.From, ChatID: chatName, To: ToFrom.To });
  });

  socket.on('AnswerRequest', async (Data) => {
  const userId = await ReturnUserID(Data.From);
  console.log('ID: ' + userId);
  if (Data.Answer) {
     await InsertChat(Data.ChatID);
     await socket.join(Data.ChatID);
     await UpdateChat(Data.ChatID, {
        chatRoom: Data.ChatID,
        content: 'User ' + Data.To + ' connected!',
        sender: 'Chat'
     });
    await InsertChatToUser(Data.To, Data.ChatID, Data.From);
    await AddUserToChat(Data.ChatID, Data.To);
    await io.to(userId).emit('RequestAnswer', { ChatID: Data.ChatID, Answer: true, To: Data.To, From: Data.From });
  } else {
    await io.to(userId).emit('RequestAnswer', { ChatID: Data.ChatID, Answer: false, To: Data.To });
  }
});
 socket.on('ConnectSecUserToChat', async (Data) => {
     await socket.join(Data.ChatID);
     console.log('Trying to connect seccond user!')
     await UpdateChat(Data.ChatID, {
        chatRoom: Data.ChatID,
        content: 'User ' + Data.From + ' connected!',
        sender: 'Chat'
     });
    await InsertChatToUser(Data.From, Data.ChatID, Data.To);
    await AddUserToChat(Data.ChatID, Data.From);
    await io.to(Data.ChatID).emit('updateInfo');
  } 
);

  socket.on('disconnect', () => {
    console.log('A user disconnected');
  });
});

const PORT = 3001;
server.listen(PORT, (err) => {
  if (err) {
    console.error(err);
  } else {
    console.log(`Server listening on port ${PORT}`);
  }
});
