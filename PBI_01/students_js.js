var row_index = 0 ;

function addStudentShow() {
    var add_student_dialog = document.getElementById("form_student_id");
    var label_top = document.getElementById('student_label_id');
    label_top.innerHTML = 'Add Student';
    add_student_dialog.style.visibility='visible';
    var confirm_on_form = document.getElementById('confirm_on_form')
    confirm_on_form.setAttribute('onclick', 'addStudentToTable()');
    add_student_dialog.showModal();// Set other class name
}
function addStudentToTable(){
    var group = document.getElementById('group_input').value
    var name = document.getElementById('name_input').value
    var secname = document.getElementById('secname_input').value
    var gender = document.getElementById('gender_input').value
    var birthday = document.getElementById('birthday_input').value
    this.addRows(group, name, secname, gender, birthday)
    this.closeForm('form_student_id')
}
function addRows(group, name, secname, gender, birthday) {
    var table = document.getElementById('my_students_table');
    var rowCount = table.rows.length;
    var row = table.insertRow(rowCount);
    var cell_checkbox = row.insertCell(0);
    var cell_group = row.insertCell(1);
    var cell_name = row.insertCell(2);
    var cell_gender = row.insertCell(3);
    var cell_birthday = row.insertCell(4);
    var cell_status = row.insertCell(5);
    var cell_options = row.insertCell(6);
    var input_checkbox = document.createElement("input")
    input_checkbox.setAttribute("type", "checkbox")
    input_checkbox.setAttribute("class", "col_checkbox")
    cell_checkbox.appendChild(input_checkbox);
    cell_group.innerHTML = group;
    cell_name.innerHTML = secname + ' ' + name;
    cell_gender.innerHTML = gender;
    cell_birthday.innerHTML = birthday;
    var status = document.createElement("div");
    status.setAttribute('class', 'dot');
    status.setAttribute('id', 'table_dot_active');
    cell_status.appendChild(status);
    var options_pen = document.createElement("i");
    options_pen.setAttribute('class', 'fa-solid fa-pen fa-xl');
    options_pen.setAttribute('onclick', 'editStudentShow(this)');
    cell_options.appendChild(options_pen);
    var options_delete = document.createElement("i");
    options_delete.setAttribute('class', 'fa-regular fa-square-minus fa-2xl');
    options_delete.setAttribute('onclick', 'deleteStudentShow(this)');
    cell_options.appendChild(options_delete);
}

function editStudentShow(clicked_button){
    row_index = this.get_row_index(clicked_button)
    var text = document.getElementById('student_label_id');
    text.innerHTML = 'Edit Student';
    var edit_student_dialog = document.getElementById("form_student_id");
    var table = document.getElementById('my_students_table');
    var my_group = document.getElementById('group_input');
    var my_name = document.getElementById('name_input');
    var my_secname = document.getElementById('secname_input');
    var my_gender = document.getElementById('gender_input');
    var my_birthday= document.getElementById('birthday_input');
    var confirm_on_form = document.getElementById('confirm_on_form')
    confirm_on_form.setAttribute('onclick', 'editStudent()');
    my_group.value = table.rows[row_index].cells[1].innerHTML;
    var name_secname_array = table.rows[row_index].cells[2].innerHTML;
    name_secname_array = name_secname_array.split(' ');
    my_name.value = name_secname_array[row_index];
    my_secname.value = name_secname_array[0];
    my_gender.value = table.rows[row_index].cells[3].innerHTML;
    my_birthday.value = table.rows[row_index].cells[4].innerHTML;
    edit_student_dialog.style.visibility='visible';
    edit_student_dialog.showModal();
}
function editStudent(){
    var table = document.getElementById('my_students_table');
    table.rows[row_index].cells[1].innerHTML = document.getElementById('group_input').value;
    table.rows[row_index].cells[2].innerHTML = document.getElementById('secname_input').value + ' ' + document.getElementById('name_input').value;
    table.rows[row_index].cells[3].innerHTML = document.getElementById('gender_input').value;
    table.rows[row_index].cells[4].innerHTML = document.getElementById('birthday_input').value;
    this.closeForm('form_student_id');
}
function deleteStudent(){
    document.getElementById('my_students_table').deleteRow(row_index);
    this.closeForm('delete_student_form_id')
}
function deleteStudentShow(clicked_button){
    row_index = this.get_row_index(clicked_button);
    var table = document.getElementById('my_students_table');
    var delete_student_dialog = document.getElementById("delete_student_form_id");
    var my_label = document.getElementById('text_warning');
    var name_secname_array = table.rows[row_index].cells[2].innerHTML;
    name_secname_array = name_secname_array.split(' ');
    my_label.textContent = "Are you sure you want to delete user " + name_secname_array[0] + ' '+ name_secname_array[1] + '?';
    delete_student_dialog.style.visibility='visible';
    delete_student_dialog.showModal();
}
function closeForm(id){
    var my_form = document.getElementById(id);
    my_form.style.visibility='hidden';
    my_form.close();
    document.getElementById('group_input').selectedIndex = 0;
    document.getElementById('name_input').value = '';
    document.getElementById('secname_input').value = '';
    document.getElementById('gender_input').selectedIndex = 0;;
    document.getElementById('birthday_input').value = '';

}
function checkAllCheck(){
    const checkbox = document.getElementsByClassName('col_checkbox')
    const maincheckbox = document.getElementById('main_checkbox')
    if(maincheckbox.checked == true){
        for (var i = 0; i < checkbox.length; i++){
            checkbox[i].checked = false;
        }
    }
    else{
        for (var i = 0; i < checkbox.length; i++){
            checkbox[i].checked = true;
        }
    }
}
function get_row_index(button){
    var row = button.parentNode.parentNode; // Get the row containing the button
    var rowIndex = row.rowIndex; // Get the index of the row
    return rowIndex;
}





