let row_index = 0 ;
let user_index = 0 ; 
let group_input = document.getElementById('group_input')
let name_input = document.getElementById('name_input')
let secname_input = document.getElementById('secname_input')
let gender_input = document.getElementById('gender_input')
let birthday_input = document.getElementById('birthday_input')
let add_student_dialog = document.getElementById("form_student_id")
let label_top = document.getElementById('student_label_id')
let confirm_on_form = document.getElementById('confirm_on_form')
confirm_on_form.setAttribute('onclick', 'addStudentToTable()');
let edit_student_dialog = document.getElementById("form_student_id");
let table = document.getElementById('my_students_table');
let delete_student_dialog = document.getElementById("delete_student_form_id");
let my_label = document.getElementById('text_warning');
let UserID_on_form = document.getElementById('userId');
let group_error = document.getElementById('group_error')
let name_error = document.getElementById('name_error')
let secname_error = document.getElementById('secname_error')
let gender_error = document.getElementById('gender_error')
let birthday_error = document.getElementById('birthday_error')

let deleteArray = [];
let users_array = []


function addStudentShow() {
    label_top.innerHTML = 'Add Student';
    confirm_on_form.setAttribute('onclick', 'addStudentToTable()');
    add_student_dialog.style.visibility='visible';
    add_student_dialog.showModal();// Set other class name
    UserID_on_form.value =  user_index; // Index to addStudentForm
    group_error.style.display = 'none' ;
    name_error.style.display = 'none' ;
    secname_error.style.display = 'none' ;
    gender_error.style.display = 'none' ;
    birthday_error.style.display = 'none' ;
}
function addStudentToTable(){
    let Check = ValidateFormWhenApply();
    if(!Check) {
        this.addRows(group_input.value, name_input.value, secname_input.value, gender_input.value, birthday_input.value)
        this.closeForm('form_student_id')
    }
}
function ValidateFormWhenApply(){
    let is_prevent = 0;
    if(group_input.selectedIndex === 0 ){
        group_error.style.display = 'block' ;
        is_prevent+=1;
    }
    else{
        group_error.style.display = 'none' ;
    }
    if (!/[A-Za-zа-яіА-я ]+/.test(name_input.value) || name_input.value.trim().length === 0  ) {
        name_error.style.display = 'block' ;
        is_prevent+=1;
    }
    else{
        name_error.style.display = 'none' ;
    }
    if (!/[A-Za-zа-яіА-я ]+/.test(secname_input.value) || name_input.value.trim().length === 0) {
        secname_error.style.display = 'block' ;
        is_prevent+=1;
    }
    else{
        secname_error.style.display = 'none' ;
    }
    if(gender_input.selectedIndex === 0 ){
        gender_error.style.display = 'block' ;
        is_prevent+=1;
    }
    else{
        gender_error.style.display = 'none' ;
    }
    if (birthday_input.value >= '2008-01-01' || birthday_input.value <= '1900-01-01') {
        birthday_error.style.display = 'block';
        is_prevent+=1;
    }
    if(is_prevent){
        return 1;
    }
}

function addRows(group, name, secname, gender, birthday) {
    let table = document.getElementById('my_students_table');
    let rowCount = table.rows.length;
    let row = table.insertRow(rowCount);
    row.id = user_index; // Index to row
    user_index+=1 ; // Increment Index
    let cell_checkbox = row.insertCell(0);
    let cell_group = row.insertCell(1);
    let cell_name = row.insertCell(2);
    let cell_gender = row.insertCell(3);
    let cell_birthday = row.insertCell(4);
    let cell_status = row.insertCell(5);
    let cell_options = row.insertCell(6);
    let input_checkbox = document.createElement("input")
    input_checkbox.setAttribute("type", "checkbox")
    input_checkbox.setAttribute("class", "col_checkbox")
    cell_checkbox.appendChild(input_checkbox);
    cell_group.innerHTML = group;
    cell_name.innerHTML = secname + ' ' + name;
    cell_gender.innerHTML = gender;
    cell_birthday.innerHTML = birthday;
    let status = document.createElement("div");
    status.setAttribute('class', 'dot');
    status.setAttribute('id', 'table_dot_active');
    cell_status.appendChild(status);
    let options_pen = document.createElement("i");
    options_pen.setAttribute('class', 'fa-solid fa-pen fa-xl');
    options_pen.setAttribute('onclick', 'editStudentShow(this)');
    cell_options.appendChild(options_pen);
    let options_delete = document.createElement("i");
    options_delete.setAttribute('class', 'fa-regular fa-square-minus fa-2xl');
    options_delete.setAttribute('onclick', 'deleteStudentShow(this)');
    cell_options.appendChild(options_delete);
    this.createJSON(user_index - 1, group_input.value, name_input.value, secname_input.value, gender_input.value, birthday_input.value)
}
function createJSON(id, group, name, secname, gender, birthday){
    let studentJSON = JSON.stringify({'id': id, "group" : group, "name" : name, "second_name" : secname, "gender" : gender, "birthday" : birthday});
    console.log(studentJSON);
}
function editStudentShow(clicked_button){
    group_error.style.display = 'none' ;
    name_error.style.display = 'none' ;
    secname_error.style.display = 'none' ;
    gender_error.style.display = 'none' ;
    birthday_error.style.display = 'none' ;
    row_index = this.get_row_index(clicked_button)
    let text = document.getElementById('student_label_id');
    text.innerHTML = 'Edit Student';
    confirm_on_form.setAttribute('onclick', 'editStudentInTable()');
    UserID_on_form.setAttribute('value', table.rows[row_index].id);
    group_input.value = table.rows[row_index].cells[1].innerHTML;
    let name_secname_array = table.rows[row_index].cells[2].innerHTML;
    name_secname_array = name_secname_array.split(' ');
    name_input.value = name_secname_array[1];
    secname_input.value = name_secname_array[0];
    gender_input.value = table.rows[row_index].cells[3].innerHTML;
    birthday_input.value = table.rows[row_index].cells[4].innerHTML;
    edit_student_dialog.style.visibility='visible';
    edit_student_dialog.showModal();
}
function editStudentInTable(){
    let Check = ValidateFormWhenApply();
    if(!Check) {
        table.rows[row_index].cells[1].innerHTML = group_input.value;
        table.rows[row_index].cells[2].innerHTML = secname_input.value + ' ' + name_input.value;
        table.rows[row_index].cells[3].innerHTML = gender_input.value;
        table.rows[row_index].cells[4].innerHTML = birthday_input.value;
        this.createJSON(table.rows[row_index].id, group_input.value, name_input.value, secname_input.value, gender_input.value, birthday_input.value);
        this.closeForm('form_student_id');
    }
}
function deleteStudent(){
    let decrement = 0;
    if(deleteArray.length > 1){
        for (let i of deleteArray){
            document.getElementById('my_students_table').deleteRow(i - decrement);
            console.log(i)
            decrement += 1;
        }
    }
    else{
        document.getElementById('my_students_table').deleteRow(row_index);
    }
    this.closeForm('delete_student_form_id')
}
function deleteStudentShow(clicked_button){
    deleteArray = [];
    const checkbox = document.getElementsByClassName('col_checkbox')
    for (let i = 1; i < checkbox.length; i++){
        if(checkbox[i].checked === true){
            deleteArray.push(i);
        }
    }
    if(deleteArray.length > 1){
        my_label.textContent = "Are you sure you want to delete multiple users?";
    }
    else{
        row_index = this.get_row_index(clicked_button);
        let name_secname_array = table.rows[row_index].cells[2].innerHTML;
        name_secname_array = name_secname_array.split(' ');
        my_label.textContent = "Are you sure you want to delete user " + name_secname_array[0] + ' '+ name_secname_array[1] + '?';
    }
    delete_student_dialog.style.visibility='visible';
    delete_student_dialog.showModal();

}
function closeForm(id){
    let my_form = document.getElementById(id);
    my_form.style.visibility='hidden';
    my_form.close();
    group_input.selectedIndex = 0;
    name_input.value = '';
    secname_input.value = '';
    gender_input.selectedIndex = 0;
    birthday_input.value = '';

}
function checkAllCheck(){
    const checkbox = document.getElementsByClassName('col_checkbox')
    const maincheckbox = document.getElementById('main_checkbox')
    if(maincheckbox.checked == true){
        for (let i = 0; i < checkbox.length; i++){
            checkbox[i].checked = false;
        }
    }
    else{
        for (let i = 0; i < checkbox.length; i++){
            checkbox[i].checked = true;
        }
    }
}
function get_row_index(button){
    let row = button.parentNode.parentNode; // Get the row containing the button
    let rowIndex = row.rowIndex; // Get the index of the row
    return rowIndex;
}

function GroupError(){
    if(group_input.selectedIndex === 0 ){
        group_error.style.display = 'block' ;
        is_prevent+=1;
    }
    else{
        group_error.style.display = 'none' ;
    }
}
function NameError(){
    if (!/[A-Za-zа-яіА-я ]$/.test(name_input.value)) {
        name_error.style.display = 'block' ;
    }
    else{
        name_error.style.display = 'none' ;
    }
}
function SecNameError(){
    if (!/[A-Za-zа-яіА-я ]$/.test(secname_input.value)) {
        secname_error.style.display = 'block' ;
    }
    else{
        secname_error.style.display = 'none' ;
    }
}
function GenderError(){
    if(gender_input.selectedIndex === 0 ){
        gender_error.style.display = 'block' ;
        is_prevent+=1;
    }
    else{
        gender_error.style.display = 'none' ;
    }
}
function CheckBirthday(){
    if (birthday_input.value >= '2008-01-01' || birthday_input.value <= '1900-01-01') {
        birthday_error.style.display = 'block';
    }
    else {
        birthday_error.style.display = 'none';
    }
}
if('serviceWorker' in navigator){
    navigator.serviceWorker.register('sw.js')
        .then((reg) => console.log('ServiceWorker has been REGISTERED'))
        .catch((err) => {console.log('ServiceWorker has NOT been REGISTERED');console.log(err)}
        );
}


