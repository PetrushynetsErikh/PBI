<?php

$json = file_get_contents('php://input');
$data = json_decode($json, true);

$id = $data['id'];
$group = $data['group'];
$name = $data['name'];
$second_name = $data['second_name'];
$gender = $data['gender'];
$birthday = $data['birthday'];
if(empty($data['group']) || !isset($data['group'])  || empty($data['name']) || !isset($data['name']) || empty($data['second_name']) || !isset($data['second_name']) || empty($data['gender']) || !isset($data['gender']) || empty($data['birthday']) || !isset($data['birthday'])){
    $result = array(
        'status' => false,
        'error' => array(
            'code' => 500,
            'message' => "Internal server error"
        )
    );
    header('Content-Type: application/json');
    echo json_encode($result);
    exit;
}
else{
    $result = array(
        'status' => true,
        'message' => $data
    );
    header('Content-Type: application/json');
    echo json_encode($result);
    exit;

}


?>
