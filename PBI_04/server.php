<?php
$servername = '127.0.0.1';
$username = 'root';
$password = '';
$db = 'studentsdb';

try{
    $conn = new mysqli($servername, $username, $password, $db, 3307);
}
catch(Exception $e) {
    $result = array(
        'status' => false,
        'error' => array(
            'code' => 500,
            'message' => "Cant connect to Database!!!"
        )
    );
    $jsonData = json_encode($result);
    header('Content-Type: application/json');
    echo $jsonData;
    die();
}
$json = file_get_contents('php://input');
$data = json_decode($json, true);
$action = $data['action'];
    if($action == 'g'){
        $sql = "SELECT sID, sGroup, sName, sSurname, sGender, sBirthday FROM students_table";
        $result = mysqli_query($conn, $sql);
        if(mysqli_num_rows($result) >= 1){
            $myArray = array();
            while ($row = mysqli_fetch_assoc($result)) {
                $dict = array(
                    "sID" => $row["sID"],
                    "sGroup" => $row["sGroup"],
                    "sName" => $row["sName"],
                    "sSurname" => $row["sSurname"],
                    "sGender" => $row["sGender"],
                    "sBirthday" => $row["sBirthday"]
                );
                // Add the dictionary to the array
                $myArray[] = $dict;
            }
            $result = array(
                'status' => true,
                'message' => $myArray, "Returned INFO!"
            );
        }
        else{
            $result = array(
                'status' => true,
                'message' => "Database is empty"
            );
        }
    }
    else if($action == 'a'){
        $id = $data['id'];
        $group = $data['group'];
        $name = $data['name'];
        $second_name = $data['second_name'];
        $gender = $data['gender'];
        $birthday = $data['birthday'];
        if(!preg_match("/^[a-zA-Z0-9а-яА-я\s-]+$/", $data['group']) || !preg_match("/^[a-zA-Zа-яА-я\s']+$/", $data['name']) || !preg_match("/^[a-zA-Zа-яА-я\s']+$/", $data['second_name'])  || !preg_match("/^[a-zA-Zа-яА-я\s']+$/", $data['gender']) || !isset($data['birthday'])){
            $result = array(
                'status' => false,
                'error' => array(
                    'code' => 500,
                    'message' => "Please, fill fields correctly!"
                )
            );
            header('Content-Type: application/json');
            $jsonData = json_encode($result);
            echo $jsonData;
            die();
        }
        $sql = "INSERT INTO students_table (sID, sGroup, sName, sSurname, sGender, sBirthday)
        VALUES ($id,'$group','$name','$second_name','$gender','$birthday')";
        mysqli_query($conn, $sql);
        $result = array(
            'status' => true,
            'message' => $data , "Succesfully Added!"
        );
    }
    else if($action == 'e'){
        $id = $data['id'];
        $group = $data['group'];
        $name = $data['name'];
        $second_name = $data['second_name'];
        $gender = $data['gender'];
        $birthday = $data['birthday'];
        if(!preg_match("/^[a-zA-Z0-9а-яА-я\s-]+$/", $data['group']) || !preg_match("/^[a-zA-Zа-яА-я\s']+$/", $data['name']) || !preg_match("/^[a-zA-Zа-яА-я\s']+$/", $data['second_name'])  || !preg_match("/^[a-zA-Zа-яА-я\s']+$/", $data['gender']) || !isset($data['birthday'])){
            $result = array(
                'status' => false,
                'error' => array(
                    'code' => 500,
                    'message' => "Please, fill fields correctly!"
                )
            );

        }
        $sql = "UPDATE students_table 
                SET  sGroup = '$group', sName = '$name', sSurname = '$second_name' , sGender = '$gender', sBirthday = '$birthday' 
                WHERE sID = $id";
        mysqli_query($conn, $sql);
        $result = array(
            'status' => true,
            'message' => $data , "Succesfully Edited!"
        );
    }
    else if($action == 'd'){
        $id = $data['id'];
        $sql = "DELETE FROM students_table
                WHERE sID = $id";
        mysqli_query($conn, $sql);
        $result = array(
            'status' => true,
            'message' => $data , "Succesfully Deleted!"
        );
    }
header('Content-Type: application/json');
$jsonData = json_encode($result);
echo $jsonData;
?>