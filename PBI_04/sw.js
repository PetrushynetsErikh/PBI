var CACHE_NAME = 'Cache_01';
var urlsToCache = ['students_table.css','students_table.js','students_table.html'];
self.addEventListener('install', event=>{
    event.waitUntil(caches.open(CACHE_NAME).then(cache=>{
            return cache.addAll(urlsToCache);
        })
    );
    console.log('ServiceWorker has been INSTALED!');
});
self.addEventListener('activate', event=> {
    console.log('ServiceWorker has been ACTIVATED!');
});

self.addEventListener('fetch', event=> {
    event.respondWith(
        fetch(event.request).then(function(networkResponse) {
            return networkResponse
        })
    )
});



