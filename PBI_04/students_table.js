let row_index = 0 ;
let user_index = 0 ; 
let group_input = document.getElementById('group_input')
let name_input = document.getElementById('name_input')
let secname_input = document.getElementById('secname_input')
let gender_input = document.getElementById('gender_input')
let birthday_input = document.getElementById('birthday_input')
let add_student_dialog = document.getElementById("form_student_id")
let label_top = document.getElementById('student_label_id')
let confirm_on_form = document.getElementById('confirm_on_form')
confirm_on_form.setAttribute('onclick', 'addStudentToTable()');
let edit_student_dialog = document.getElementById("form_student_id");
let table = document.getElementById('my_students_table');
let delete_student_dialog = document.getElementById("delete_student_form_id");
let my_label = document.getElementById('text_warning');
let UserID_on_form = document.getElementById('userId');
let group_error = document.getElementById('group_error')
let name_error = document.getElementById('name_error')
let secname_error = document.getElementById('secname_error')
let gender_error = document.getElementById('gender_error')
let birthday_error = document.getElementById('birthday_error')
let delete_row_index = 0 ;
let deleteArray = [];

let getINFO = createdeleteJSON('g', '-1');
fetch('server.php', {
    method: 'POST',
    headers: {
        'Content-Type': 'application/json'
    },
    body: getINFO
})
    .then(response => response.json())
    .then(responseData => {
        if(responseData['status'] === true && responseData['message'] !=='Database is empty'){
            console.log('Success!');
            for(let student of responseData['message']){
                console.log(student);
                addRows(student['sGroup'] , student['sName'], student['sSurname'], student['sGender'], student['sBirthday'], student['sID'], 'load')
            }
            user_index = 1 + parseInt(responseData['message'][responseData['message'].length - 1]['sID']) ;
        }
        else if(responseData['status'] === true && responseData['message'] ==='Database is empty'){
            console.log(responseData['message']);
        }
        else{
            console.log('Error: ' + responseData.status);
            swal("Error occured!", responseData['error']['message'], "error");
            closeForm('form_student_id')
        }
        console.log('JSON data received from PHP server:', responseData);
    })
    .catch(error => console.error('Error sending JSON data:', error));

function addStudentShow() {
    label_top.innerHTML = 'Add Student';
    confirm_on_form.setAttribute('onclick', 'addStudentToTable()');
    add_student_dialog.style.visibility='visible';
    add_student_dialog.showModal();// Set other class name
    UserID_on_form.value =  user_index; // Index to addStudentForm
    group_error.style.display = 'none' ;
    name_error.style.display = 'none' ;
    secname_error.style.display = 'none' ;
    gender_error.style.display = 'none' ;
    birthday_error.style.display = 'none' ;
}
function addStudentToTable(){
    //let Check = ValidateFormWhenApply();
    let Check = 0 ;
    if(!Check) {
        let studentJSON = createJSON('a', user_index, group_input.value, name_input.value, secname_input.value, gender_input.value, birthday_input.value);
        fetch('server.php', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: studentJSON
        })
            .then(response => response.json())
            .then(responseData => {
                if (responseData['status'] === true) {
                    addRows(group_input.value, name_input.value, secname_input.value, gender_input.value, birthday_input.value, user_index, 'add')
                    closeForm('form_student_id')
                } else {
                    console.log('Error: ' + responseData.status);
                    swal("Student wasn't added!", responseData['error']['message'], "error");
                    closeForm('form_student_id')
                }
                console.log('JSON data received from PHP server:', responseData);
            })
            .catch(error => console.error('Error sending JSON data:', error));
    }

}
function ValidateFormWhenApply(){
    let is_prevent = 0;
    if(group_input.selectedIndex === 0 ){
        group_error.style.display = 'block' ;
        is_prevent+=1;
    }
    else{
        group_error.style.display = 'none' ;
    }
    if (!/[A-Za-zа-яіА-я ]+/.test(name_input.value) || name_input.value.trim().length === 0  ) {
        name_error.style.display = 'block' ;
        is_prevent+=1;
    }
    else{
        name_error.style.display = 'none' ;
    }
    if (!/[A-Za-zа-яіА-я ]+/.test(secname_input.value) || name_input.value.trim().length === 0) {
        secname_error.style.display = 'block' ;
        is_prevent+=1;
    }
    else{
        secname_error.style.display = 'none' ;
    }
    if(gender_input.selectedIndex === 0 ){
        gender_error.style.display = 'block' ;
        is_prevent+=1;
    }
    else{
        gender_error.style.display = 'none' ;
    }
    if (birthday_input.value >= '2008-01-01' || birthday_input.value <= '1900-01-01') {
        birthday_error.style.display = 'block';
        is_prevent+=1;
    }
    if(is_prevent){
        return 1;
    }
}

function addRows(group, name, secname, gender, birthday, index, type) {
    console.log('Inside addRows',group, name, secname);
    let table = document.getElementById('my_students_table');
    let rowCount = table.rows.length;
    let row = table.insertRow(rowCount);
    if(type == 'load'){
        row.id = parseInt(index); // Index to row
    }
    else{
        row.id = user_index; // Index to row
        user_index += 1 ; // Increment Index
    }
    let cell_checkbox = row.insertCell(0);
    let cell_group = row.insertCell(1);
    let cell_name = row.insertCell(2);
    let cell_gender = row.insertCell(3);
    let cell_birthday = row.insertCell(4);
    let cell_status = row.insertCell(5);
    let cell_options = row.insertCell(6);
    let input_checkbox = document.createElement("input")
    input_checkbox.setAttribute("type", "checkbox")
    input_checkbox.setAttribute("class", "col_checkbox")
    cell_checkbox.appendChild(input_checkbox);
    cell_group.innerHTML = group;
    cell_name.innerHTML = secname + ' ' + name;
    cell_gender.innerHTML = gender;
    cell_birthday.innerHTML = birthday;
    let status = document.createElement("div");
    status.setAttribute('class', 'dot');
    status.setAttribute('id', 'table_dot_active');
    cell_status.appendChild(status);
    let options_pen = document.createElement("i");
    options_pen.setAttribute('class', 'fa-solid fa-pen fa-xl');
    options_pen.setAttribute('onclick', 'editStudentShow(this)');
    cell_options.appendChild(options_pen);
    let options_delete = document.createElement("i");
    options_delete.setAttribute('class', 'fa-regular fa-square-minus fa-2xl');
    options_delete.setAttribute('onclick', 'deleteStudentShow(this)');
    cell_options.appendChild(options_delete);
}
function createJSON(action, id, group, name, secname, gender, birthday){
    let studentJSON = JSON.stringify({'action': action, 'id': id, "group" : group, "name" : name, "second_name" : secname, "gender" : gender, "birthday" : birthday});
    console.log(studentJSON);
    return studentJSON;
}
function createdeleteJSON(action, id){
    let studentJSON = JSON.stringify({'action': action, 'id': id});
    console.log(studentJSON);
    return studentJSON;
}
function editStudentShow(clicked_button){
    group_error.style.display = 'none' ;
    name_error.style.display = 'none' ;
    secname_error.style.display = 'none' ;
    gender_error.style.display = 'none' ;
    birthday_error.style.display = 'none' ;
    row_index = this.get_row_index(clicked_button)
    let row = document.getElementById(row_index);
    let text = document.getElementById('student_label_id');
    text.innerHTML = 'Edit Student';
    confirm_on_form.setAttribute('onclick', 'editStudentInTable()');
    console.log('From EDITSTUNDENT: ', row.id);
    UserID_on_form.setAttribute('value', row.id);
    group_input.value = row.cells[1].innerHTML;
    let name_secname_array = row.cells[2].innerHTML;
    name_secname_array = name_secname_array.split(' ');
    name_input.value = name_secname_array[1];
    secname_input.value = name_secname_array[0];
    gender_input.value = row.cells[3].innerHTML;
    birthday_input.value = row.cells[4].innerHTML;
    edit_student_dialog.style.visibility='visible';
    edit_student_dialog.showModal();
}
function editStudentInTable(){
    //let Check = ValidateFormWhenApply();
    let Check = 0 ;
    let row = document.getElementById(row_index);
    if(!Check) {
        studentJSON = this.createJSON('e', row.id, group_input.value, name_input.value, secname_input.value, gender_input.value, birthday_input.value);
        fetch('server.php', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: studentJSON
        })
            .then(response => response.json())
            .then(responseData => {
                if(responseData['status'] === true){
                    row.cells[1].innerHTML = group_input.value;
                    row.cells[2].innerHTML = secname_input.value + ' ' + name_input.value;
                    row.cells[3].innerHTML = gender_input.value;
                    row.cells[4].innerHTML = birthday_input.value;
                    closeForm('form_student_id')
                }
                else{
                    console.log('Error: ' + responseData.status);
                    swal("Student wasn't edited!", responseData['error']['message'], "error");
                    closeForm('form_student_id')
                }
                console.log('JSON data received from PHP server:', responseData);
            })
            .catch(error => console.error('Error sending JSON data:', error));
    }
}
function deleteStudent(clicked_button){
    let table = document.getElementById('my_students_table'); // Get the table element
    if(deleteArray.length > 1){
        for (let i of deleteArray){
            studentJSON = this.createJSON('d', i);
            fetch('server.php', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: studentJSON
            })
                .then(response => response.json())
                .then(responseData => {
                    if(responseData['status'] === true){
                        let row = document.querySelector(`#${'my_students_table'} tr[id="${i}"]`);
                        row.parentNode.removeChild(row);
                        console.log(i);
                        this.closeForm('delete_student_form_id')
                    }
                    else{
                        console.log('Error: ' + responseData.status);
                        swal("Student wasn't deleted!", responseData['error']['message'], "error");
                        this.closeForm('delete_student_form_id')
                    }
                    console.log('JSON data received from PHP server:', responseData);
                })
                .catch(error => console.error('Error sending JSON data:', error));
        }
    }
    else{
        console.log(delete_row_index);
        studentJSON = this.createdeleteJSON('d', delete_row_index);
        fetch('server.php', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: studentJSON
        })
            .then(response => response.json())
            .then(responseData => {
                if(responseData['status'] === true){
                    let row = document.querySelector(`#${'my_students_table'} tr[id="${delete_row_index}"]`); // Get the row to delete
                    row.parentNode.removeChild(row); // Remove the row from the table
                    closeForm('delete_student_form_id')

                }
                else{
                    console.log('Error: ' + responseData.status);
                    swal("Student wasn't deleted!", responseData['error']['message'], "error");
                }
                console.log('JSON data received from PHP server:', responseData);
                closeForm('delete_student_form_id')

            })
            .catch(error => console.error('Error sending JSON data:', error));
        closeForm('delete_student_form_id')
    }
}
function deleteStudentShow(clicked_button){
    deleteArray = [];
    const checkbox = document.getElementsByClassName('col_checkbox')
    for (let i = 1; i < checkbox.length; i++){
        if(checkbox[i].checked === true){
            row_index = this.get_row_index(checkbox[i]);
            deleteArray.push(row_index);
        }
    }
    if(deleteArray.length > 1){
        my_label.textContent = "Are you sure you want to delete multiple users?";
    }
    else{
        delete_row_index = this.get_row_index(clicked_button);
        let row = document.querySelector(`#${'my_students_table'} tr[id="${delete_row_index}"]`); // Get the row to delete
        let name_secname_array = row.cells[2].innerHTML;
        name_secname_array = name_secname_array.split(' ');
        my_label.textContent = "Are you sure you want to delete user " + name_secname_array[0] + ' '+ name_secname_array[1] + '?';
    }
    delete_student_dialog.style.visibility='visible';
    delete_student_dialog.showModal();
}
function closeForm(id){
    let my_form = document.getElementById(id);
    my_form.style.visibility='hidden';
    my_form.close();
    group_input.selectedIndex = 0;
    name_input.value = '';
    secname_input.value = '';
    gender_input.selectedIndex = 0;
    birthday_input.value = '';
}
function checkAllCheck(){
    const checkbox = document.getElementsByClassName('col_checkbox')
    const maincheckbox = document.getElementById('main_checkbox')
    if(maincheckbox.checked == true){
        for (let i = 0; i < checkbox.length; i++){
            checkbox[i].checked = false;
        }
    }
    else{
        for (let i = 0; i < checkbox.length; i++){
            checkbox[i].checked = true;
        }
    }
}
function get_row_index(button){
    let row = button.parentNode.parentNode; // Get the row containing the button
    let rowIndex = row.id; // Get the index of the row
    return rowIndex;
}

function GroupError(){
    if(group_input.selectedIndex === 0 ){
        group_error.style.display = 'block' ;
        is_prevent+=1;
    }
    else{
        group_error.style.display = 'none' ;
    }
}
function NameError(){
    if (!/[A-Za-zа-яіА-я ]$/.test(name_input.value)) {
        name_error.style.display = 'block' ;
    }
    else{
        name_error.style.display = 'none' ;
    }
}
function SecNameError(){
    if (!/[A-Za-zа-яіА-я ]$/.test(secname_input.value)) {
        secname_error.style.display = 'block' ;
    }
    else{
        secname_error.style.display = 'none' ;
    }
}
function GenderError(){
    if(gender_input.selectedIndex === 0 ){
        gender_error.style.display = 'block' ;
        is_prevent+=1;
    }
    else{
        gender_error.style.display = 'none' ;
    }
}
function CheckBirthday(){
    if (birthday_input.value >= '2008-01-01' || birthday_input.value <= '1900-01-01') {
        birthday_error.style.display = 'block';
    }
    else {
        birthday_error.style.display = 'none';
    }
}
if('serviceWorker' in navigator){
    navigator.serviceWorker.register('sw.js')
        .then((reg) => console.log('ServiceWorker has been REGISTERED'))
        .catch((err) => {console.log('ServiceWorker has NOT been REGISTERED');console.log(err)}
        );
}

